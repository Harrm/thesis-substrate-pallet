use frame_support::sp_std::marker::PhantomData;
use frame_support::sp_std::ops::{SubAssign, AddAssign};

trait Task {
    type Params;
    fn activate(&self, params: Self::Params);
    fn rollback(&self, params: Self::Params);
}

struct ChangeAttributeTask<'a, AttrT, AmountT> {
    amount: AmountT,
    phantom: PhantomData<&'a AttrT>
}

impl<'a, T: Copy, AttrT: Attribute<T>> Task for ChangeAttributeTask<'a, AttrT, T> {
    type Params = &'a mut AttrT;

    fn activate(&self, attr: Self::Params) {
        *attr += self.amount;
    }

    fn rollback(&self, _attr: Self::Params) {
        unimplemented!()
    }
}

trait Attribute<T>: SubAssign<T> + AddAssign<T> {
    fn set(&mut self, val: T);
    fn get(&self) -> &T;
}

impl<T: SubAssign<T> + AddAssign<T>> Attribute<T> for T {
    fn set(&mut self, val: T) {
        *self = val;
    }

    fn get(&self) -> &T {
        self
    }
}

trait AttributeSet {
}

struct CharacterAttributeSet {
    health: u16
}

impl AttributeSet for CharacterAttributeSet {
}

trait Ability {
    type TargetAttributeSet;

    fn apply(&self, attribute_set: &mut Self::TargetAttributeSet);
}

struct DamageAbility {
    amount: u16
}

impl Ability for DamageAbility {
    type TargetAttributeSet = CharacterAttributeSet;

    fn apply(&self, attribute_set: &mut CharacterAttributeSet) {
        attribute_set.health -= self.amount;
    }

}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn attribute_change() {
        let mut a = 10;
        let t1 = ChangeAttributeTask { amount: -3, phantom: Default::default() };
        t1.activate(&mut a);
        assert_eq!(a, 7);

        let t2 = ChangeAttributeTask { amount: 3, phantom: Default::default() };
        t2.activate(&mut a);
        assert_eq!(a, 10);
    }

    #[test]
    fn test_ability() {
        let a = DamageAbility { amount: 3 };
        let mut s = CharacterAttributeSet { health: 10 };
    }
}