mod ability;

type PlayerId = u64;
type AssetId = u64;
type ActorId = u64;

struct Player {
    id: PlayerId
}

struct Asset {
    id: AssetId,
    owner: PlayerId
}

struct Actor {
    id: ActorId,
    player: PlayerId
}


#[cfg(test)]
mod tests {

    #[test]
    fn construct_player() {

    }

}